/*
 * =====================================================================================
 *
 *       Filename:  zip.TDD.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/06/2018 05:05:00 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE psycho/zip.hpp
#include <boost/test/unit_test.hpp>

#include <psycho/zip.hpp>

#include <string>
#include <vector>

#include <boost/filesystem.hpp>

BOOST_AUTO_TEST_SUITE( test_zip_hpp )

BOOST_AUTO_TEST_CASE( unzip_to_directory ) {
	using namespace psycho::zip;
	namespace bf=boost::filesystem;
	using namespace std;

	auto tmp_zip_file=bf::unique_path();
	auto tmp_dir=bf::unique_path();

	vector<string> file_data;
	vector<string> file_name;

	file_data.push_back("aaaaaaaabbbbb\n\n\n\n\nsadadasdasd\n");file_name.push_back("test/test.txt");
	file_data.push_back("");file_name.push_back("test2.txt");
	file_data.push_back("");file_name.push_back("test3.txt");
	file_data.push_back("");file_name.push_back("test3.txt");

	BOOST_TEST_MESSAGE( "Creating .zip file with "<<file_name.size()<<" files");
	{
		Zip_writer w(tmp_zip_file.native());

		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			auto zf=w.add_file(file_name[i]);
			*zf << file_data[i];
			BOOST_CHECK( not zf->bad() );
		}
	}
	BOOST_REQUIRE( bf::exists(tmp_zip_file) );
	BOOST_CHECK_NE( bf::file_size(tmp_zip_file), 0 );

	bf::create_directories(tmp_dir);
	BOOST_REQUIRE( unzip(tmp_zip_file.native(), tmp_dir.native()) );

	BOOST_TEST_MESSAGE("Check if files is created");
	for( unsigned int i=0 ; i<file_name.size() ; ++i){
		BOOST_REQUIRE( bf::exists(tmp_dir.native()+bf::path::preferred_separator+file_name[i]) );
	}

	bf::remove(tmp_zip_file);
	bf::remove_all(tmp_dir);
}

BOOST_AUTO_TEST_SUITE_END()

