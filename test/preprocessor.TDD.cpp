/*
 * =====================================================================================
 *
 *       Filename:  preprocessor.TDD.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/07/2018 02:37:15 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *
 * =====================================================================================
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE psycho/preprocessor.hpp
#include <boost/test/unit_test.hpp>

#include <psycho/preprocessor.hpp>

#include <string>

template<typename T>
class Foo{
	psy_declare_gs(T,data);
};

BOOST_AUTO_TEST_SUITE( test_declaration )

BOOST_AUTO_TEST_CASE( get_and_set_char ) {
	Foo<char> x;
	x.data('S');
	BOOST_REQUIRE_EQUAL( x.data(), 'S' );
}

BOOST_AUTO_TEST_CASE( get_and_set_string ) {
	using namespace std;

	Foo<string> x;
	string test_string="Test Test Test";

	x.data(test_string);
	BOOST_REQUIRE_EQUAL( x.data(), test_string );
}

BOOST_AUTO_TEST_CASE( get_and_set_int ) {
	Foo<int> x;
	x.data(1000);
	BOOST_REQUIRE_EQUAL( x.data(), 1000 );

	x.data(0);
	BOOST_REQUIRE_EQUAL( x.data(), 0 );
}

BOOST_AUTO_TEST_SUITE_END()

