/*
 * =====================================================================================
 *
 *       Filename:  stream.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/05/2018 04:22:50 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *
 * =====================================================================================
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE psycho/stream.hpp
#include <boost/test/unit_test.hpp>

#include <sstream>
#include <string>

#include <psycho/stream.hpp>

BOOST_AUTO_TEST_SUITE( test_stream_to_stream )

BOOST_AUTO_TEST_CASE( test_read ) {
	using namespace std;
	using namespace psycho::stream;

	string i("test test test test test test");

	ostringstream out;
	istringstream in(i);

	stream_to_stream( in, out);

	BOOST_CHECK_EQUAL( out.str(), i);
}

BOOST_AUTO_TEST_SUITE_END()

