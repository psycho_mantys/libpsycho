/*
 * =====================================================================================
 *
 *       Filename:  Zip_reader.TDD.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/05/2018 08:28:22 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *         
 * =====================================================================================
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE psycho/Zip_reader.hpp and psycho/Zip_writer.hpp
#include <boost/test/unit_test.hpp>

#include <psycho/Zip_reader.hpp>
#include <psycho/Zip_writer.hpp>

#include <psycho/stream.hpp>

#include <string>
#include <sstream>

#include <boost/filesystem.hpp>

BOOST_AUTO_TEST_SUITE( test_zip_reader_and_writer )

BOOST_AUTO_TEST_CASE( write_and_read_1_zip_files ) {
	using namespace psycho::zip;
	using namespace psycho::stream;
	namespace bf=boost::filesystem;
	using namespace std;

	auto tmp_zip_file=bf::unique_path();
	BOOST_TEST_MESSAGE( "tmp_zip_file: "<< tmp_zip_file.native() );

	string data1="test123456789\n\n\n\n\nxxxxxx";
	
	{
		Zip_writer w(tmp_zip_file.native());

		{
			auto zf=w.add_file("test1.txt");
			*zf << data1;
			BOOST_CHECK( not zf->bad() );
		}
	}
	BOOST_CHECK_NE( bf::file_size(tmp_zip_file), 0 );
	{
		Zip_reader r(tmp_zip_file.native());
		auto file_list=r.file_list();
		BOOST_CHECK( file_list.size()==1 );
		{
			ostringstream data;
			auto zf=r.get_file("test1.txt");

			stream_to_stream( *zf, data);

			BOOST_CHECK_EQUAL( data1, data.str() );
		}
	}
	bf::remove(tmp_zip_file);
}

BOOST_AUTO_TEST_CASE( write_and_read_2_zip_files ) {
	using namespace psycho::zip;
	using namespace psycho::stream;
	namespace bf=boost::filesystem;
	using namespace std;

	auto tmp_zip_file=bf::unique_path();
	BOOST_TEST_MESSAGE( "tmp_zip_file: "<< tmp_zip_file.native() );

	vector<string> file_data;
	vector<string> file_name;
	
	file_data.push_back("test123456789\n\n\n\n\nxxxxxx");file_name.push_back("test1.txt");
	file_data.push_back("0987654321sssaaasa\naaaa\n\n\n\n\nxxxxx\n\n\n\n\nyyyyyy");file_name.push_back("test2.txt");
	
	BOOST_TEST_MESSAGE( "Creating .zip file with "<<file_name.size()<<" files");
	{
		Zip_writer w(tmp_zip_file.native());

		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			auto zf=w.add_file(file_name[i]);
			*zf << file_data[i];
			BOOST_CHECK( not zf->bad() );
		}
	}

	BOOST_REQUIRE( bf::exists(tmp_zip_file) );
	BOOST_CHECK_NE( bf::file_size(tmp_zip_file), 0 );

	{
		Zip_reader r(tmp_zip_file.native());
		auto file_list=r.file_list();

		BOOST_CHECK_EQUAL( file_list.size(), file_name.size() );
		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			ostringstream data;
			auto zf=r.get_file(file_name[i]);

			stream_to_stream( *zf, data);

			BOOST_CHECK_EQUAL( file_data[i], data.str() );
		}
	}
	bf::remove(tmp_zip_file);
}

BOOST_AUTO_TEST_CASE( write_and_read_3_zip_files ) {
	using namespace psycho::zip;
	using namespace psycho::stream;
	namespace bf=boost::filesystem;
	using namespace std;

	auto tmp_zip_file=bf::unique_path();
	BOOST_TEST_MESSAGE( "tmp_zip_file: "<< tmp_zip_file.native() );

	vector<string> file_data;
	vector<string> file_name;
	
	file_data.push_back("test123456789\n\n\n\n\nxxxxxx");file_name.push_back("test1.txt");
	file_data.push_back("");file_name.push_back("test2.txt");
	file_data.push_back("0987654321sssaaasa\naaaa\n\n\n\n\nxxxxx\n\n\n\n\nyyyyyy");file_name.push_back("test3.txt");
	
	BOOST_TEST_MESSAGE( "Creating .zip file with "<<file_name.size()<<" files");
	{
		Zip_writer w(tmp_zip_file.native());

		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			auto zf=w.add_file(file_name[i]);
			*zf << file_data[i];
			BOOST_CHECK( not zf->bad() );
		}
	}

	BOOST_REQUIRE( bf::exists(tmp_zip_file) );
	BOOST_CHECK_NE( bf::file_size(tmp_zip_file), 0 );

	{
		Zip_reader r(tmp_zip_file.native());
		auto file_list=r.file_list();

		BOOST_CHECK_EQUAL( file_list.size(), file_name.size() );
		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			ostringstream data;
			auto zf=r.get_file(file_name[i]);

			stream_to_stream( *zf, data);

			BOOST_CHECK_EQUAL( file_data[i], data.str() );
		}
	}
	bf::remove(tmp_zip_file);

}

BOOST_AUTO_TEST_CASE( write_and_read_1_empty_file_from_zip_file ) {
	using namespace psycho::zip;
	using namespace psycho::stream;
	namespace bf=boost::filesystem;
	using namespace std;

	auto tmp_zip_file=bf::unique_path();
	BOOST_TEST_MESSAGE( "tmp_zip_file: "<< tmp_zip_file.native() );

	vector<string> file_data;
	vector<string> file_name;

	file_data.push_back("");file_name.push_back("test1.txt");

	BOOST_TEST_MESSAGE( "Creating .zip file with "<<file_name.size()<<" files");
	{
		Zip_writer w(tmp_zip_file.native());

		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			auto zf=w.add_file(file_name[i]);
			*zf << file_data[i];
			BOOST_CHECK( not zf->bad() );
		}
	}

	BOOST_REQUIRE( bf::exists(tmp_zip_file) );
	BOOST_CHECK_NE( bf::file_size(tmp_zip_file), 0 );

	{
		Zip_reader r(tmp_zip_file.native());
		auto file_list=r.file_list();

		BOOST_CHECK_EQUAL( file_list.size(), file_name.size() );
		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			ostringstream data;
			auto zf=r.get_file(file_name[i]);

			stream_to_stream( *zf, data);

			BOOST_CHECK_EQUAL( file_data[i], data.str() );
		}
	}
	bf::remove(tmp_zip_file);
}


BOOST_AUTO_TEST_CASE( write_and_read_3_empty_files_from_zip_file ) {
	using namespace psycho::zip;
	using namespace psycho::stream;
	namespace bf=boost::filesystem;
	using namespace std;

	auto tmp_zip_file=bf::unique_path();
	BOOST_TEST_MESSAGE( "tmp_zip_file: "<< tmp_zip_file.native() );

	vector<string> file_data;
	vector<string> file_name;

	file_data.push_back("");file_name.push_back("test1.txt");
	file_data.push_back("");file_name.push_back("test2.txt");
	file_data.push_back("");file_name.push_back("test3.txt");

	BOOST_TEST_MESSAGE( "Creating .zip file with "<<file_name.size()<<" files");
	{
		Zip_writer w(tmp_zip_file.native());

		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			auto zf=w.add_file(file_name[i]);
			*zf << file_data[i];
			BOOST_CHECK( not zf->bad() );
		}
	}

	BOOST_REQUIRE( bf::exists(tmp_zip_file) );
	BOOST_CHECK_NE( bf::file_size(tmp_zip_file), 0 );

	{
		Zip_reader r(tmp_zip_file.native());
		auto file_list=r.file_list();
		BOOST_CHECK_EQUAL( file_list.size(), file_name.size() );
		for( unsigned int i=0 ; i<file_name.size() ; ++i){
			ostringstream data;
			auto zf=r.get_file(file_name[i]);

			stream_to_stream( *zf, data);

			BOOST_CHECK_EQUAL( file_data[i], data.str() );
		}
	}
	bf::remove(tmp_zip_file);
}

BOOST_AUTO_TEST_SUITE_END()

