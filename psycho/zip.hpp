/*
 * =====================================================================================
 *
 *       Filename:  zip.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/04/2018 07:53:12 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef __PSYCHO_ZIP_HPP
#define __PSYCHO_ZIP_HPP

#include <psycho/Zip_reader.hpp>
#include <psycho/Zip_writer.hpp>

#include <string>
#include <vector>

namespace psycho {
namespace zip {

bool zip(std::string zip_filename, const std::vector<std::string> &file_list);

bool unzip(const std::string &zip_filename, const std::string &dir="");

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

#endif // __PSYCHO_ZIP_HPP

