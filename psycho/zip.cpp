/*
 * =====================================================================================
 *
 *       Filename:  zip.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/04/2018 08:54:49 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#include <psycho/zip.hpp>
#include <psycho/stream.hpp>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/convenience.hpp>

namespace psycho {
namespace zip {

bool zip(std::string zip_filename, const std::vector<std::string> &file_list){
	using namespace std;
	using namespace psycho::stream;

	Zip_writer zip_file(zip_filename);
 
	for( unsigned int i=0 ; i<file_list.size() ; ++i ){
		ifstream file( file_list[i], ios::in|ios::binary );
		if( file.is_open() ){
			auto zf=zip_file.add_file( file_list[i] );
			stream_to_stream(file, *zf);
		}else{
			return false;
		}
	}
	return true;
}

bool unzip(const std::string &zip_filename, const std::string &dir){
	using namespace std;
	using namespace psycho::stream;
	namespace bf=boost::filesystem;

	Zip_reader zip_file(zip_filename);

	auto file_list=zip_file.file_list();

	for(int i=0; i<file_list.size() ; ++i){
		string out_filename=dir+bf::path::preferred_separator+file_list[i];

		// Recursive create directories
		if( not bf::create_directories( bf::path(out_filename).parent_path() ) ){
			runtime_error("Error create_directories(....)");
		}

		ofstream fout( out_filename, ios::out|ios::binary );
		auto in=zip_file.get_file(file_list[i]);

		stream_to_stream(*in, fout);
	}
	return true;
}

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

