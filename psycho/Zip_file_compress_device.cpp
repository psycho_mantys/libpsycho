/*
 * =====================================================================================
 *
 *       Filename:  Zip_file_decompress_device.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/30/2018 07:01:08 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

/* #####   HEADER FILE INCLUDES   ################################################### */
#include <psycho/Zip_file_compress_device.hpp>


#include <time.h>
#include <minizip/zip.h>
#include <minizip/ioapi.h>

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/categories.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/convenience.hpp>

#include <iostream>
#include <stdexcept>

namespace psycho {
namespace zip {

/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE   ################################### */

/* #####   TYPE DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ######################### */

/* #####   DATA TYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */

/* #####   CLASS DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ######################## */

/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ################################ */

/* #####   PROTOTYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */

/* #####   FUNCTION DEFINITIONS  -  EXPORTED FUNCTIONS   ############################ */

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */
void get_minizip_time(tm_zip& tz);

/* #####   CLASS IMPLEMENTATIONS  -  EXPORTED CLASSES   ############################# */
/*
Zip_file_compress_device::Zip_file_compress_device(std::string zip_filename, std::string filename_on_zip) :
	filename_on_zip(filename_on_zip), zip_filename(zip_filename)
{
	using namespace std;
	//cout << "ctor" << endl;
	buffer_in=std::shared_ptr<vector <char_type> >(new std::vector<char_type>());
	int error=0;
	zip_source=zip_open(zip_filename.c_str(), ZIP_CREATE, &error);
	if( error!=0 or zip_source==NULL ){
		throw BOOST_IOSTREAMS_FAILURE(zip_strerror(zip_source));
	}
	//zf_target=zip_fopen(zip_source, filename_on_zip.c_str(), 0);
	//if( zf_target==NULL ){
	//	throw BOOST_IOSTREAMS_FAILURE(zip_strerror(zip_source));
	//}
}

Zip_file_compress_device::Zip_file_compress_device(const Zip_file_compress_device &c){
	using namespace std;
	//cout << "ctor cpy" << endl;
	zip_filename=c.zip_filename;
	Zip_file_compress_device *cc=const_cast<Zip_file_compress_device*>(&c);
	zip_source=cc->zip_source;
	cc->zip_source=NULL;
	filename_on_zip=c.filename_on_zip;
	buffer_in=c.buffer_in;
}

std::streamsize Zip_file_compress_device::write(const char_type* s, std::streamsize n){
	using namespace std;

	//return zip_fread( zf_target, s, n);
	//cout << "write" << endl;
	for( int i=0 ; i<n ; ++i){
		const char_type c=((const char*)(s)+i)[0];
		buffer_in->push_back(c);
	}
	return n;
}

Zip_file_compress_device::~Zip_file_compress_device(){
	//std::cout << "dctor ZFC" << std::endl;
	zip_source_t *s=NULL;

	//std::cout << "buffer_in:" << *buffer_in << "|" << std::endl;

	if( zip_source!=NULL ){
		//std::cout << "buffer_in.size():" << buffer_in->size() << "|" << std::endl;
		if( (s=zip_source_buffer(zip_source, &((*buffer_in)[0]), buffer_in->size(), 0))==NULL||
			zip_file_add(zip_source, filename_on_zip.c_str(), s, ZIP_FL_OVERWRITE)<0 ){
			//zip_file_add(zip_source, filename_on_zip.c_str(), s, ZIP_FL_OVERWRITE)<0 ){
			printf("error adding file: %s\n", zip_strerror(zip_source));
			zip_source_free(s);
		}
		zip_close(zip_source);
	}
	//std::cout << "dctor ZFC end" << std::endl;
	//zip_fclose(zf_target);
}
*/























// MINIZIP IMPL.

MINIZip_file_compress_device::MINIZip_file_compress_device(std::string zip_filename, std::string filename_on_zip) :
	filename_on_zip(filename_on_zip), zip_filename(zip_filename)
{
	using namespace std;
	namespace bf=boost::filesystem;
	//cout << "ctor " <<  zip_filename << endl;

	int err=0;

	try{
		if( bf::exists(zip_filename) ){
			//cout << "exists()==true" << endl;
			zip_source=zipOpen64( zip_filename.c_str(), APPEND_STATUS_ADDINZIP);
		}else{
			//cout << "exists()==false" << endl;
			zip_source=zipOpen64( zip_filename.c_str(), APPEND_STATUS_CREATE);
		}

		if( zip_source==NULL ){
			runtime_error("Error zipOpen64(....)");
		}

		zip_fileinfo zi={0};
		get_minizip_time( zi.tmz_date );

		int err=zipOpenNewFileInZip( zip_source, filename_on_zip.c_str(), &zi,
			NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION );
		if( err ){
			runtime_error("Error zipOpenNewFileInZip(....)");
		}
	}catch(exception &e){
		if( err ){
			zipClose( zip_source, 0 );	
		}
		if( zip_source!=NULL ){
			zipCloseFileInZip( zip_source );
		}
		throw;
	}
}

MINIZip_file_compress_device::MINIZip_file_compress_device(const MINIZip_file_compress_device &c){
	using namespace std;
	//cout << "ctor cpy" << endl;

	zip_filename=c.zip_filename;
	MINIZip_file_compress_device *cc=const_cast<MINIZip_file_compress_device*>(&c);
	zip_source=cc->zip_source;
	cc->zip_source=NULL;
	filename_on_zip=c.filename_on_zip;
}

std::streamsize MINIZip_file_compress_device::write(const char_type* s, std::streamsize n){
	using namespace std;
	//return zip_fread( zf_target, s, n);
	//cout << "write" << endl;

	int err=0;

	err=zipWriteInFileInZip( zip_source, s, n );
	if( err!=ZIP_OK ){
		return 0;
	}
	return n;
}

MINIZip_file_compress_device::~MINIZip_file_compress_device(){
	//std::cout << "dctor ZFC" << std::endl;

	if( zip_source!=NULL ){
		zipCloseFileInZip( zip_source );
		zipClose( zip_source, 0 );
		zip_source=NULL;
	}
	//std::cout << "dctor ZFC end" << std::endl;
}



/* #####   EXPORTED VARIABLES   ##################################################### */

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */

/* #####   CLASS IMPLEMENTATIONS  -  LOCAL CLASSES   ################################ */
void get_minizip_time(tm_zip& tz){

	time_t raw_time;
	time(&raw_time);

	auto timeinfo=localtime(&raw_time);

	tz.tm_year=timeinfo->tm_year;
	tz.tm_mday=timeinfo->tm_mday;
	tz.tm_hour=timeinfo->tm_hour;
	tz.tm_min=timeinfo->tm_min;
	tz.tm_sec=timeinfo->tm_sec;
	tz.tm_mon=timeinfo->tm_mon;
}

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

