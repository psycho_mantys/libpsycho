/*
 * =====================================================================================
 *
 *       Filename:  zip.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/29/2018 03:57:05 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef __PSYCHO_ZIP_READER_HPP
#define __PSYCHO_ZIP_READER_HPP


#include <vector>
#include <string>
#include <cassert>
#include <iostream>
#include <memory>


namespace psycho {
namespace zip {

class Zip_reader{
	public:
		Zip_reader(const std::string& filename);
		~Zip_reader();

		std::shared_ptr<std::istream> get_file(const std::string& filename);
		const std::vector<std::string> &file_list();
		
		void init_file_list();

	private:
		std::string path;
		
		bool lazy_init_file_list;

		std::vector<std::string> _file_list;
		std::vector<std::string> _folder_list;
};

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

#endif // __PSYCHO_ZIP_READER_HPP

