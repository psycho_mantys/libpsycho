/*
 * =====================================================================================
 *
 *       Filename:  Zip_reader.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/30/2018 02:05:55 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include <psycho/Zip_reader.hpp>
#include <psycho/Zip_file_decompress_device.hpp>

#include <minizip/zip.h>
#include <minizip/ioapi.h>

#include <vector>
#include <string>
#include <istream>
#include <memory>

namespace psycho {
namespace zip {

void Zip_reader::init_file_list(){
	using namespace std;

	_file_list.clear();
	_folder_list.clear();

	unzFile zip_source;
	
	int err=0;

	try{
		zip_source=unzOpen64( path.c_str() );
		if( zip_source==NULL ){
			runtime_error("Error unzOpen64(....)");
		}
	}catch(exception &e){
		throw;
	}

	unz_global_info64 global_info;
	err=unzGetGlobalInfo64( zip_source, &global_info );
	for ( unsigned long i=0; i<global_info.number_entry && err==UNZ_OK; ++i ){
		char filename[FILENAME_MAX];
		unz_file_info64 file_info;

		err=unzGetCurrentFileInfo64( zip_source, &file_info, filename,
			sizeof(filename), NULL, 0, NULL, 0);
		if( err==UNZ_OK ){
			const char last_c=filename[file_info.size_filename-1];
			if ( last_c=='/' || last_c=='\\' ){
				_folder_list.push_back(filename);
			}else{
				_file_list.push_back(filename);
			}
			err=unzGoToNextFile(zip_source);
		}
	}
	if( zip_source!=NULL ){
		unzCloseCurrentFile(zip_source);
		unzClose(zip_source);
	}
	lazy_init_file_list=false;
}

const std::vector<std::string> &Zip_reader::file_list(){
	using namespace std;
	if( lazy_init_file_list ){
		init_file_list();
	}
	return _file_list;
}

Zip_reader::Zip_reader(const std::string &filename):
	path(filename),
	lazy_init_file_list(true)
{
}

Zip_reader::~Zip_reader(){
}

std::shared_ptr<std::istream> Zip_reader::get_file(const std::string& filename){
	namespace io=boost::iostreams;

	std::istream *is=new io::stream<MINIZip_file_decompress_device>(path, filename);
	std::shared_ptr<std::istream> ret(is);

	return ret;
}

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

