/*
 * =====================================================================================
 *
 *       Filename:  preprocessor.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/07/2018 02:33:36 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *
 * =====================================================================================
 */
#ifndef __PSYCHO_PREPROCESSOR_HPP
#define __PSYCHO_PREPROCESSOR_HPP

#define psy_declare_gs(type, var_name) \
	private:\
		type _##var_name; \
	public: \
		void var_name(const type& var){\
			_##var_name = var;\
		}\
\
		const type& var_name(){\
			return _##var_name; \
		}

#endif // __PSYCHO_PREPROCESSOR_HPP

