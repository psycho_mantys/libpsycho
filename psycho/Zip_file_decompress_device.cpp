/*
 * =====================================================================================
 *
 *       Filename:  Zip_file_decompress_device.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/30/2018 07:01:08 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

/* #####   HEADER FILE INCLUDES   ################################################### */
#include <psycho/Zip_file_decompress_device.hpp>

#include <minizip/unzip.h>
#include <minizip/ioapi.h>

#include <iostream>

/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE   ################################### */

/* #####   TYPE DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ######################### */

/* #####   DATA TYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */

/* #####   CLASS DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ######################## */

/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ################################ */

/* #####   PROTOTYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */

/* #####   FUNCTION DEFINITIONS  -  EXPORTED FUNCTIONS   ############################ */

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */

/* #####   CLASS IMPLEMENTATIONS  -  EXPORTED CLASSES   ############################# */

/*
Zip_file_decompress_device::Zip_file_decompress_device(std::string zip_filename, std::string filename_on_zip) :
	zip_filename(zip_filename), filename_on_zip(filename_on_zip)
{
	int error=0;
	zip_source=zip_open(zip_filename.c_str(), ZIP_RDONLY, &error);
	if( error!=0 or zip_source==NULL ){
		throw BOOST_IOSTREAMS_FAILURE(zip_strerror(zip_source));
	}
	zf_target=zip_fopen(zip_source, filename_on_zip.c_str(), 0);
}

Zip_file_decompress_device::Zip_file_decompress_device(const Zip_file_decompress_device &c){
	using namespace std;
	cout << "ctor cpy" << endl;
	zip_source=c.zip_source;
	zf_target=c.zf_target;
	filename_on_zip=c.filename_on_zip;
	zip_filename=c.zip_filename;

	Zip_file_decompress_device *cc=const_cast<Zip_file_decompress_device*>(&c);
	cc->zip_source=NULL;
	cc->zf_target=NULL;
}

std::streamsize Zip_file_decompress_device::read(void* s, std::streamsize n){
	return zip_fread( zf_target, s, n);
}

void Zip_file_decompress_device::close(){
	if( zf_target!=NULL ){
		zip_fclose(zf_target);
		zf_target=NULL;
	}
	if( zip_source!=NULL ){
		zip_close(zip_source);
		zip_source=NULL;
	}
}



*/








///// MINIZip
namespace psycho {
namespace zip {

MINIZip_file_decompress_device::MINIZip_file_decompress_device(std::string zip_filename, std::string filename_on_zip) :
	filename_on_zip(filename_on_zip), zip_filename(zip_filename), zip_source(NULL)
{
	using namespace std;
	//cout << "ctor()" << endl;

	int err=0;
	try{
		zip_source=unzOpen64( zip_filename.c_str() );
		if( zip_source==NULL ){
			runtime_error("Error unzOpen64(....)");
		}
		err=unzLocateFile( zip_source, filename_on_zip.c_str(), 0 );
		if( err!=UNZ_OK ){
			runtime_error("Error unzLocateFile(....)");
		}
		err=unzOpenCurrentFile(zip_source);
		if( err!=UNZ_OK ){
			runtime_error("Error unzOpenCurrentFile(....)");
		}
	}catch(exception &e){
		close();
		throw;
	}
}

MINIZip_file_decompress_device::MINIZip_file_decompress_device(const MINIZip_file_decompress_device &c){
	using namespace std;
	//cout << "ctor cpy" << endl;
	zip_source=c.zip_source;

	filename_on_zip=c.filename_on_zip;
	zip_filename=c.zip_filename;

	MINIZip_file_decompress_device *cc=const_cast<MINIZip_file_decompress_device*>(&c);
	cc->zip_source=NULL;
}

std::streamsize MINIZip_file_decompress_device::read(char_type* s, std::streamsize n){
	using namespace std;
	//cout << "read()" << endl;
	//cout << "read()" << filename_on_zip << " " << n << "lido " << lido << endl;
	return unzReadCurrentFile( zip_source, s, n );
}

void MINIZip_file_decompress_device::close(){
	using namespace std;
	//cout << "close()" << endl;
	if( zip_source!=NULL ){
		//cout << "true close()" << endl;
		unzCloseCurrentFile(zip_source);
		unzClose(zip_source);
		zip_source=NULL;
	}
}

MINIZip_file_decompress_device::~MINIZip_file_decompress_device(){
	using namespace std;
	//cout << "dtor()" << endl;
	close();
}

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */



/* #####   EXPORTED VARIABLES   ##################################################### */

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */

/* #####   CLASS IMPLEMENTATIONS  -  LOCAL CLASSES   ################################ */

