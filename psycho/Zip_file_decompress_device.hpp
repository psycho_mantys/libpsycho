/*
 * =====================================================================================
 *
 *       Filename:  Zip_file_decompress_device.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/30/2018 06:12:38 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Psycho Mantys
 *
 * =====================================================================================
 */
#ifndef __PSYCHO_ZIP_FILE_DECOMPRESS_DEVICE_HPP
#define __PSYCHO_ZIP_FILE_DECOMPRESS_DEVICE_HPP

/* #####   HEADER FILE INCLUDES   ################################################### */
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/categories.hpp>

#include <minizip/unzip.h>
#include <minizip/ioapi.h>

/* #####   EXPORTED MACROS   ######################################################## */

/* #####   EXPORTED TYPE DEFINITIONS   ############################################## */

/* #####   EXPORTED DATA TYPES   #################################################### */

/* #####   EXPORTED CLASS DEFINITIONS   ############################################# */
/*
class Zip_file_decompress_device{
	public:
		// Boost::iostream magic definitions. See on boost docs.
		typedef char char_type;
		typedef boost::iostreams::source_tag	category;

		Zip_file_decompress_device(std::string filename, std::string filename_on_zip);
		Zip_file_decompress_device(const Zip_file_decompress_device &c);

		std::streamsize read(void* s, std::streamsize n);

		void close();
	private:
		//Zip_file_decompress_device operator=(const Zip_file_decompress_device&);


		struct zip_file *zf_target;
		struct zip *zip_source;
		std::string filename_on_zip;
		
		std::string zip_filename;
};
*/






namespace psycho {
namespace zip {

class MINIZip_file_decompress_device{
	public:
		// Boost::iostream magic definitions. See on boost docs.
		typedef char char_type;
		typedef boost::iostreams::source_tag	category;

		MINIZip_file_decompress_device(std::string zip_filename, std::string filename_on_zip);
		MINIZip_file_decompress_device(const MINIZip_file_decompress_device &c);

		std::streamsize read(char_type* s, std::streamsize n);

		void close();
		
		~MINIZip_file_decompress_device();
	private:
		//Zip_file_decompress_device operator=(const Zip_file_decompress_device&);

		std::string zip_filename;
		unzFile zip_source;
		std::string filename_on_zip;
};

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

/* #####   EXPORTED VARIABLES   ##################################################### */

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */

#endif // __PSYCHO_ZIP_FILE_DECOMPRESS_DEVICE_HPP

