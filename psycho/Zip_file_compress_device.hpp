/*
 * =====================================================================================
 *
 *       Filename:  Zip_file_compress_device.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/30/2018 06:12:38 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Psycho Mantys
 *
 * =====================================================================================
 */
#ifndef __PSYCHO_ZIP_FILE_COMPRESS_DEVICE_HPP
#define __PSYCHO_ZIP_FILE_COMPRESS_DEVICE_HPP

// http://www.fileformat.info/format/zip/corion.htm
// https://en.wikipedia.org/wiki/Zip_%28file_format%29

/* #####   HEADER FILE INCLUDES   ################################################### */
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/concepts.hpp>
#include <boost/iostreams/categories.hpp>

#include <minizip/zip.h>
#include <minizip/ioapi.h>

namespace psycho {
namespace zip {

/* #####   EXPORTED MACROS   ######################################################## */

/* #####   EXPORTED TYPE DEFINITIONS   ############################################## */

/* #####   EXPORTED DATA TYPES   #################################################### */

/* #####   EXPORTED CLASS DEFINITIONS   ############################################# */

/* 
class Zip_file_compress_device{
	public:
		// Boost::iostream magic definitions. See on boost docs.
		typedef char char_type;
		typedef boost::iostreams::sink_tag	category;

		Zip_file_compress_device(std::string zip_filename, std::string filename_on_zip);
		Zip_file_compress_device(const Zip_file_compress_device &c);

		std::streamsize write(const char_type* s, std::streamsize n);

		~Zip_file_compress_device();
	private:
		Zip_file_compress_device operator=(const Zip_file_compress_device&);

		std::string zip_filename;
		struct zip *zip_source;
		std::string filename_on_zip;

		std::shared_ptr<std::vector<char_type> > buffer_in;
};
*/

class MINIZip_file_compress_device{
	public:
		// Boost::iostream magic definitions. See on boost docs.
		typedef char char_type;
		typedef boost::iostreams::sink_tag	category;

		MINIZip_file_compress_device(std::string zip_filename, std::string filename_on_zip);
		MINIZip_file_compress_device(const MINIZip_file_compress_device &c);

		std::streamsize write(const char_type* s, std::streamsize n);

		~MINIZip_file_compress_device();
	private:
		MINIZip_file_compress_device operator=(const MINIZip_file_compress_device&);

		std::string zip_filename;
		zipFile zip_source;
		std::string filename_on_zip;
};

/* #####   EXPORTED VARIABLES   ##################################################### */

/* #####   EXPORTED FUNCTION DECLARATIONS   ######################################### */

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

#endif // __PSYCHO_ZIP_FILE_COMPRESS_DEVICE_HPP

