/*
 * =====================================================================================
 *
 *       Filename:  stream.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/05/2018 03:28:20 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include <psycho/stream.hpp>

#include <istream>
#include <ostream>

namespace psycho {
namespace stream {

bool stream_to_stream(std::istream &in, std::ostream &out, const int buffer_size){
	using namespace std;

	char buf[buffer_size];

	in.read(buf, buffer_size);
	int read_len=in.gcount();
	while( read_len ){
		out.write(buf, read_len);
		if( out.bad() ){
			return false;
		}
		in.read(buf, buffer_size);
		if( in.bad() ){
			return false;
		}
		read_len=in.gcount();
	}

	return true;
}

}	/* -----  end of namespace stream  ----- */
}	/* -----  end of namespace psycho  ----- */

