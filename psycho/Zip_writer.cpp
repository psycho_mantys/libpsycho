/*
 * =====================================================================================
 *
 *       Filename:  Zip_reader.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/30/2018 02:05:55 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include <psycho/Zip_writer.hpp>
#include <psycho/Zip_file_compress_device.hpp>

#include <vector>
#include <string>
#include <istream>
#include <memory>


namespace psycho {
namespace zip {

Zip_writer::Zip_writer(const std::string &filename): path(filename) {
}

Zip_writer::~Zip_writer(){
	//std::cout << "Zip_writer::~Zip_writer()" << std::endl;
}

std::shared_ptr<std::ostream> Zip_writer::add_file(const std::string& filename){
	namespace io=boost::iostreams;

	std::ostream *is=new io::stream<MINIZip_file_compress_device>(path, filename);
	std::shared_ptr<std::ostream> ret(is);

	return ret;
	//return is;
}

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

