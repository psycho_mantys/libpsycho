/*
 * =====================================================================================
 *
 *       Filename:  stream.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/05/2018 03:26:01 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *
 * =====================================================================================
 */

#ifndef __PSYCHO_STREAM_HPP
#define __PSYCHO_STREAM_HPP

#include <istream>
#include <ostream>

namespace psycho {
namespace stream {

bool stream_to_stream(std::istream &in, std::ostream &out, const int buffer_size=1024);

}	/* -----  end of namespace stream  ----- */
}	/* -----  end of namespace psycho  ----- */

#endif // __PSYCHO_STREAM_HPP

