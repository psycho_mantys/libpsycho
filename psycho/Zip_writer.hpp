/*
 * =====================================================================================
 *
 *       Filename:  zip.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/29/2018 03:57:05 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef __PSYCHO_ZIP_WRITER_HPP
#define __PSYCHO_ZIP_WRITER_HPP


#include <vector>
#include <string>
#include <cassert>
#include <iostream>
#include <memory>

namespace psycho {
namespace zip {

class Zip_writer{
	public:
		Zip_writer(const std::string& filename);
		~Zip_writer();

		std::shared_ptr<std::ostream> add_file(const std::string& filename);
	private:
		std::string path;
};

}	/* -----  end of namespace zip  ----- */
}	/* -----  end of namespace psycho  ----- */

#endif // __PSYCHO_ZIP_WRITER_HPP

